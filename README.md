# **Bijouz**

Bijouz es un sistema de publicación de productos de bijouterie. El sistema busca de manera simple y rápida realizar la compra de productos.

## **Funcionalidades del sistema**

- **Modo oscuro** : Podremos entrar en modo dark.
- **Búsqueda por categoría y color** : Podemos filtrar por tipo de bijouterie y color de preferencia.
- **Compra** : Podemos encargar el producto de forma cómoda.

![Bijouz](./doc/img/bijouz.png "Bijouz")

## **Ejecucion**

Para instalar las dependencias.

`npm install`

Para correr la aplicacion.

`npm run dev`

## **Demo**

[https://bijouz.vercel.app](https://bijouz.vercel.app "Bijouz")
