"use client";

// add bootstrap css
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";

// add normalize.css
import "normalize.css/normalize.css";

// styles
import styles from "../styles/App.module.css";

// utils
import { Body } from "../components/Body";
import { useEffect } from "react";

export default function App() {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);

  return (
    <div id="appBody" className={styles.appBody}>
      <Body />
    </div>
  );
}
