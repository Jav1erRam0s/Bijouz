import styles from "../styles/Loading.module.css";

import Image from "next/image";
import Loader from "../sources/images/loader.gif";

export default function Loading() {
  return (
    <div className={styles.containerLoading}>
      <Image
        className={styles.imgLoader}
        src={Loader}
        alt="loader"
        width={1}
        height={1}
        sizes="100vw"
      />
    </div>
  );
}
