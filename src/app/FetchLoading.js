const  fetchTest = async () => {
  // imitates delay
  return new Promise((resolve) => setTimeout(resolve, 7000));
};

export default async function FetchLoading() {
  
  const data = await fetchTest();
  
  return null;
}