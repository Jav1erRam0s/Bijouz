import "./globals.css";

import { Navegation } from "../components/Navegation";

import { Footer } from "../components/Footer";

import { Suspense } from "react";

import Loading from "./loading.js";

import FetchLoading from "./FetchLoading.js";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html>
      <head>
        <title>Bijouz</title>
      </head>
      <body>
        <Suspense fallback={<Loading />}>
          {/* Fetch Loading */}
          <FetchLoading />
          {/* Body App */}
          <Navegation />
          {children}
          <Footer />
        </Suspense>
      </body>
    </html>
  );
}
