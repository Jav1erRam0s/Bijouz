import React from "react";

import styles from "../styles/Pagination.module.css";

import { Rubik } from "next/font/google";

const rubik = Rubik({
  weight: "400",
  subsets: ["latin"],
});

export const Pagination = ({ paginas, paginaActual, updatePage }) => {

  /* Metodo de paginacion */
  function goToPage(page) {
    updatePage(page);
  }
  function previousPage() {
    const nuevaPage = paginaActual - 1;
    if (nuevaPage >= 1) {
      goToPage(nuevaPage);
    }
  }
  function nextPage() {
    const cantidadPaginas = paginas.length;
    const nuevaPage = paginaActual + 1;
    if (nuevaPage <= cantidadPaginas) {
      goToPage(nuevaPage);
    }
  }

  return (
    <section id={styles.containerPagination} className={`${rubik.className}`}>
      {paginas.length >= 2 && (
        <nav aria-label="Page navigation example">
          <ul className="nav-pagination pagination pagination-sm justify-content-center mb-0">
            <li className="page-item">
              <a href="#" className={styles.anclaje}>
                <button
                  className={`${"page-link"} ${styles.pageLink}`}
                  onClick={() => previousPage()}
                  aria-label="Previous"
                >
                  <span aria-hidden="true" className={styles.buttonControl}>
                    <b>&laquo;</b>
                  </span>
                </button>
              </a>
            </li>

            {paginas.map((element, index) => {
              return (
                <li className="page-item" key={index}>
                  <a href="#" className={styles.anclaje}>
                    <button
                      className={`${"page-link"} ${styles.pageLink}`}
                      onClick={() => goToPage(element)}
                    >
                      <span className={styles.buttonPage}>{element}</span>
                    </button>
                  </a>
                </li>
              );
            })}

            <li className="page-item">
              <a href="#" className={styles.anclaje}>
                <button
                  className={`${"page-link"} ${styles.pageLink}`}
                  onClick={() => nextPage()}
                  aria-label="Next"
                >
                  <span aria-hidden="true" className={styles.buttonControl}>
                    <b>&raquo;</b>
                  </span>
                </button>
              </a>
            </li>
          </ul>
        </nav>
      )}
    </section>
  );
};
