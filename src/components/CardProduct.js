import React from "react";

import Image from "next/image";

import styles from "../styles/CardProduct.module.css";

import { DetailsProduct } from "./DetailsProduct";

import { Rubik } from "next/font/google";

const rubik = Rubik({
  weight: "400",
  subsets: ["latin"],
});

function CardProduct({ product, cotizacionDolar }) {
  const [stateModal, setStateModal] = React.useState(false);

  function showTextCard() {
    if( product.stock === 0 ){
      return <span className={`${"badge rounded-pill text-bg-danger"}`}>Sin Stock</span>
    }
    else {
      return <span>$ {cotizarProducto()}</span>;
    }
  }

  function cotizarProducto() {
    var precioPesos = product.price * cotizacionDolar;
    //Redondeamos el total
    precioPesos = Math.round(precioPesos / 100) * 100;
    return precioPesos.toLocaleString("de-AR");
  }

  function openModal() {
    setStateModal( true );
  };

  function closeModal() {
    setStateModal( false );
  };


  return (
    <React.Fragment>
      <div
        className={`${"card"} ${styles.cardProducto}`}
        style={{ background: "#212529" }}
        onClick={openModal}
      >
        <div className={styles.containerImg}>
          <Image
            id={styles.imgProduct}
            src={product.photo[0]}
            alt={product.code}
            width={1}
            height={1}
            sizes="100vw"
            style={{ width: '100%', height: 'auto', objectFit: 'cover',   aspectRatio: '1 / 1' }}
          />
          <div className={`${rubik.className} ${styles.cardText}`}>{showTextCard()}</div>
        </div>
      </div>
      
      <DetailsProduct
        product={product}
        quotedPrice={cotizarProducto()}
        stateModal={stateModal}
        closeModal={closeModal}
      />
    </React.Fragment>
  );
}

export default CardProduct;
