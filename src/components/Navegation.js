"use client";

import React from "react";
import styles from "../styles/Navegation.module.css";
import Image from "next/image";
import Favicon from "../sources/images/favicon.png";

import { Kumar_One } from "next/font/google";

import { darkMode, lightMode } from "../utils/typeScreen.js";

const kumar_one = Kumar_One({
  weight: "400",
  subsets: ["latin"],
});

export function Navegation() {
  const [modedark, setModedark] = React.useState(true);

  function toggleCheckbox() {
    if (modedark == false) {
      setModedark(!modedark);
      lightMode();
    } else {
      setModedark(!modedark);
      darkMode();
    }
  }

  return (
    <header id={styles.header}>
      <nav id={styles.containerNav} href="#">
        <div className={styles.flexNav}>
          <a href="/#">
            <Image src={Favicon} alt="favicon" className={styles.imgFavicon} />
          </a>
          <span
            id="nameApp"
            className={`${styles.nameApp} ${kumar_one.className}`}
          >
            Bijouz
          </span>
          <input
            className={styles.push}
            type="checkbox"
            defaultChecked={modedark}
            onChange={() => toggleCheckbox()}
          />
        </div>
      </nav>
    </header>
  );
}
