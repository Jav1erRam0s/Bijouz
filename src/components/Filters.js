import React from "react";

import styles from "../styles/Filters.module.css";
import Image from "next/image";

import Color from "../sources/images/color.png";
import Categoria from "../sources/images/categoria.png";

export const Filters = ({ categoriesColors, filtrarProductos }) => {
  var keys = Array.from(categoriesColors.keys());
  const [categories] = React.useState(keys);

  var values = Array.from(categoriesColors.get("Todos"));
  const [colors, setColors] = React.useState(values);

  const [selectCategory, setSelectCategory] = React.useState("Todos");
  const [selectColor, setSelectColor] = React.useState("Todos");

  // Funciones de filtros
  function allCategory(){
    var valueCategoria = "Todos";
    var valueColor = document.getElementById("select-color").value;
    // Actualiza select(colors) por edicion de select(category)
    var newValues = Array.from(categoriesColors.get(valueCategoria));
    setColors(newValues);
    // Actualizar value selects
    setSelectCategory(valueCategoria);
    setSelectColor(valueColor);
    // Filtrar Productos
    filtrarProductos(valueCategoria, valueColor);
  }
  function allColor(){
    var valueCategoria = document.getElementById("select-categoria").value;
    var valueColor = "Todos";
    // Actualizar value select
    setSelectColor(valueColor);
    // Filtrar Productos
    filtrarProductos(valueCategoria, valueColor);
  }
  function filtrarPorCategoria() {
    var valueCategoria = document.getElementById("select-categoria").value;
    var valueColor = "Todos";
    // Actualiza select(colors) por edicion de select(category)
    var newValues = Array.from(categoriesColors.get(valueCategoria));
    setColors(newValues);
    // Actualizar value selects
    setSelectCategory(valueCategoria);
    setSelectColor(valueColor);
    // Filtrar Productos
    filtrarProductos(valueCategoria, valueColor);
  }
  function filtrarPorColor() {
    var valueCategoria = document.getElementById("select-categoria").value;
    var valueColor = document.getElementById("select-color").value;
    // Actualizar value select
    setSelectColor(valueColor);
    // Filtrar Productos
    filtrarProductos(valueCategoria, valueColor);
  }

  return (
    <div className={styles.flexFilter}>
      <Image src={Categoria} alt="categoria" className={styles.imgFilter} onClick={() => allCategory()}/>
      <select
        id="select-categoria"
        className="form-select form-select-sm"
        aria-label=".form-select-sm example"
        onChange={filtrarPorCategoria}
        value={selectCategory}
      >
        {categories.map((element, index) => {
          return (
            <option key={element} value={element}>
              {element}
            </option>
          );
        })}
      </select>

      <Image src={Color} alt="color" className={styles.imgFilter} onClick={() => allColor()}/>
      <select
        id="select-color"
        className="form-select form-select-sm"
        aria-label=".form-select-sm example"
        onChange={filtrarPorColor}
        value={selectColor}
      >
        {colors.map((element, index) => {
          return (
            <option key={element} value={element}>
              {element}
            </option>
          );
        })}
      </select>
    </div>
  );
};
