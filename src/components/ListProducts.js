"use client";

import React from "react";

import styles from "../styles/ListProduct.module.css";

import { Filters } from "./Filters";
import { Pagination } from "./Pagination";
import CardProduct from "./CardProduct.js";
import { FooterPagination } from "../components/FooterPagination";

function initializeValuesFilters(products) {
  var mapCategoriesColors = new Map();
  /* Inicializacion de selects de filtros */
  var listCategories = [];
  products.forEach((element) => {
    listCategories = listCategories.concat(element.category);
  });
  // Filtra valores unicos
  listCategories = listCategories.filter(
    (value, index, array) => array.indexOf(value) === index
  );
  listCategories = listCategories.sort();
  listCategories.push("todos");
  // ---------------------------------------------------------
  listCategories.forEach((category) => {
    var listCategoryColors = [];
    if (category != "todos") {
      products.forEach((element) => {
        if (element.category === category) {
          listCategoryColors = listCategoryColors.concat(element.color);
        }
      });
      // Filtra valores unicos
      listCategoryColors = listCategoryColors.filter(
        (value, index, array) => array.indexOf(value) === index
      );
      listCategoryColors = listCategoryColors.sort();
      listCategoryColors.push("todos");
    } else {
      products.forEach((element) => {
        listCategoryColors = listCategoryColors.concat(element.color);
      });
      // Filtra valores unicos
      listCategoryColors = listCategoryColors.filter(
        (value, index, array) => array.indexOf(value) === index
      );
      listCategoryColors = listCategoryColors.sort();
      listCategoryColors.push("todos");
    }
    // ---------------------------------------------------------
    // Editar a mayuscula la primera letra
    category = category[0].toUpperCase() + category.substring(1);
    var listCategoryColorsToUpperCase = [];
    listCategoryColors.forEach((categoryColors) => {
      listCategoryColorsToUpperCase.push(
        categoryColors[0].toUpperCase() + categoryColors.substring(1)
      );
    });
    // ---------------------------------------------------------
    mapCategoriesColors.set(category, listCategoryColorsToUpperCase);
  });
  return mapCategoriesColors;
}

function ListProducts({ products, dollar }) {
  /* Inicializacion de estados */
  const [cotizacionDolar] = React.useState(dollar.price);
  const [productos] = React.useState(products);
  const [productosFilter, setProductosFilter] = React.useState(products);
  const [cantidadProductosPage] = React.useState(24);
  const [actualPage, setActualPage] = React.useState(1);
  const [productosPage, setProductosPage] = React.useState(
    productos.slice(
      (actualPage - 1) * cantidadProductosPage,
      actualPage * cantidadProductosPage
    )
  );
  const cantidadDePaginas = Math.ceil(productos.length / cantidadProductosPage);
  var list = [];
  for (var i = 1; i <= cantidadDePaginas; i++) {
    list.push(i);
  }
  const [paginacion, setPaginacion] = React.useState(list);

  /* Initialize Filters */
  const [categoriesColors] = React.useState(initializeValuesFilters(products));

  /* Filtrar Productos */
  function filtrarProductos(category, color) {
    category = category.toLowerCase();
    color = color.toLowerCase();
    // Filtramos la lista de productos
    let newProductosFilter;
    if( category != "todos" && color != "todos" ){
      newProductosFilter = productos.filter(
        (producto) =>
          producto.category.toLowerCase() == category &&
          producto.color.includes(color)
      );
    }
    else if( category == "todos" && color != "todos" ){
      newProductosFilter = productos.filter(
        (producto) => producto.color.includes(color)
      );
    }
    else if( category != "todos" && color == "todos" ){
      newProductosFilter = productos.filter(
        (producto) => producto.category.toLowerCase() == category 
      );
    }
    else if( category == "todos" && color == "todos" ){
      newProductosFilter = productos;
    }
    // Inicializo la paginacion en la hoja 1
    let newActualPage = 1;
    // Evaluo la cantidad de paginas en la paginacion
    const cantidadPaginas = Math.ceil(
      newProductosFilter.length / cantidadProductosPage
    );
    var listPaginacion = [];
    for (var i = 1; i <= cantidadPaginas; i++) {
      listPaginacion.push(i);
    }
    // Actualizamos los estados
    setActualPage( newActualPage );
    setProductosFilter( newProductosFilter );
    setProductosPage( newProductosFilter.slice(
      (newActualPage - 1) * cantidadProductosPage,
      newActualPage * cantidadProductosPage
    ) );
    setPaginacion( listPaginacion );
  }

  /* Metodo de paginacion */
  function updatePage(page){
    setActualPage(page);
    setProductosPage(
      productosFilter.slice(
        (page - 1) * cantidadProductosPage,
        page * cantidadProductosPage
      )
    );
  }

  return (
    <React.Fragment>
      {/* FILTROS */}
      <Filters categoriesColors={categoriesColors} filtrarProductos={filtrarProductos}/>
      {/* LISTA DE PRODUCTOS */}
      <section id={styles.containerListProducts} className="row m-0">
        {productosPage.map((element, index) => {
          return (
            <span className="col-6 col-md-4 col-lg-3 mb-4" key={index}>
              <CardProduct
                product={element}
                cotizacionDolar={cotizacionDolar}
              />
            </span>
          );
        })}
      </section>
      {/* BARRA DE PAGINACION */}
      <Pagination paginas={paginacion} paginaActual={actualPage} updatePage={updatePage}/>
      {/* FOOTER / PAGINACION */}
      <FooterPagination paginaActual={actualPage} cantidadDePaginas={paginacion.length} updatePage={updatePage}/>
    </React.Fragment>
  );
}

export default ListProducts;
