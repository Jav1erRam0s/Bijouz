import styles from "../styles/Body.module.css";

import ListProducts from "../components/ListProducts.js";

import url from "../config/url.js";

const fetchProducts = () => {
  return fetch(`${url.path}/products.json`, { cache: "force-cache" }).then(
    (res) => res.json()
  );
};

const fetchDollar = () => {
  return fetch(`${url.path}/dollar.json`, { cache: "force-cache" }).then(
    (res) => res.json()
  );
};

export async function Body() {
  const products = await fetchProducts();
  const dollar = await fetchDollar();

  return (
    <div id={styles.containerBody}>
      <ListProducts products={products} dollar={dollar} />
    </div>
  );
}
