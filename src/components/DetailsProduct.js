import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";

import url from "../config/url.js";

import Image from "next/image";
import Money from "../sources/images/money.png";

import styles from "../styles/DetailsProduct.module.css";

import { Rubik } from "next/font/google";

const rubik = Rubik({
  weight: "400",
  subsets: ["latin"],
});

export const DetailsProduct = ({ product, quotedPrice, stateModal, closeModal }) => {

  const slides = product.photo.map((item, index) => {
    return (
      <div key={index}>
        {index === 0 && (
          <div className={`${"carousel-item active carousel-item-slide"} ${styles.carouselItemSlide}`}>
            <div className={styles.alignImage}>            
              <Image
                src={item}
                alt={item}
                width={1}
                height={1}
                sizes="100vw"
                className={styles.imgDetailsProduct}
                // style={{ width: 'auto', height: 'auto', objectFit: 'cover' }}
              />
            </div>
          </div>
        )}
        {index !== 0 && (
          <div className={`${"carousel-item carousel-item-slide"} ${styles.carouselItemSlide}`}>
            <div className={styles.alignImage}>
              <Image
                src={item}
                alt={index}
                width={1}
                height={1}
                sizes="100vw"
                className={styles.imgDetailsProduct}
                // style={{ width: 'auto', height: 'auto', objectFit: 'cover' }}
              />
            </div>
          </div>
        )}
      </div>
    );
  });

  function openNewWindow() {
    window.open(
      `https://wa.me/549${url.phone}/?text=•%20Código:%20${product.code}%0A•%20Precio:%20$%20${quotedPrice}%0A•%20Foto:%20${product.photo[0]}`,
      "_blank"
    );
  }

  return (
    <React.Fragment>
      <Modal isOpen={stateModal} centered className={styles.modalProduct}>
        <ModalHeader
          toggle={closeModal}
          cssModule={{ "modal-title": "w-100 text-center m-0" }}
          className={styles.modalHeader}
        >
          <div className={`${styles.modalHeaderContent} ${rubik.className}`}>
            {product.stock == 0 ? (
              <React.Fragment>
                <span className={`${"badge rounded-pill text-bg-danger"}`}>
                  Sin Stock
                </span>
                <button type="button" className="btn btn-outline-secondary btn-sm" disabled>
                  Comprar
                </button>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <span>
                  <Image
                   className={styles.imgMoney}
                   src={Money}
                   alt="Money"
                   width={1}
                   height={1}
                   sizes="100vw"
                  /> 
                  {quotedPrice}
                </span>
                <button type="button" className="btn btn-success btn-sm" onClick={openNewWindow} disabled>
                  Contacto directo
                </button>
              </React.Fragment>
            )}
          </div>
        </ModalHeader>
        <ModalBody>
          <div
            id="carousel-Actions-Details-Indicators"
            className="carousel slide"
            data-bs-interval="false"
          >
            <div className={`${"carousel-inner"} ${styles.containerImages}`}>{slides}</div>

            {product.photo.length >= 2 && (
              <div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carousel-Actions-Details-Indicators" data-bs-slide="prev">
                  <span className={`${"carousel-control-prev-icon"} ${styles.carouselControlPrevIcon}`} aria-hidden="true"></span>
                  <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carousel-Actions-Details-Indicators" data-bs-slide="next">
                  <span className={`${"carousel-control-next-icon"} ${styles.carouselControlNextIcon}`} aria-hidden="true"></span>
                  <span className="visually-hidden">Next</span>
                </button>
              </div>
            )}
          </div>
        </ModalBody>
      </Modal>
    </React.Fragment>
  );
};
