import styles from "../styles/FooterPagination.module.css";

import Image from "next/image";
import Previou from "../sources/images/previou.png";
import Next from "../sources/images/next.png";

import { Rubik } from "next/font/google";

const rubik = Rubik({
  weight: "500",
  subsets: ["latin"],
});

export const FooterPagination = ({
  paginaActual,
  cantidadDePaginas,
  updatePage,
}) => {
  /* Metodo de paginacion */
  function previousPage() {
    const nuevaPage = paginaActual - 1;
    if (nuevaPage >= 1) {
      updatePage(nuevaPage);
    }
  }
  function nextPage() {
    const nuevaPage = paginaActual + 1;
    if (nuevaPage <= cantidadDePaginas) {
      updatePage(nuevaPage);
    }
  }

  return (
    <footer id={styles.footer}>
      <div
        id={styles.footerContainer}
        className="d-flex flex-row justify-content-evenly align-items-center"
      >
        {/* Button Previou */}
        <a href="#" className={styles.anclaje}>
          <div
            className={`${styles.containerMain}`}
            onClick={() => previousPage()}
          >
            <div className={`${styles.containerControl}`}>
              <Image
                src={Previou}
                alt="previou"
                className={styles.imgControl}
              />
            </div>
          </div>
        </a>
        {/* Info */}
        <div className={`${styles.infoPages} ${rubik.className}`}>
          {paginaActual}/{cantidadDePaginas}
        </div>
        {/* Button Next */}
        <a href="#" className={styles.anclaje}>
          <div
            className={`${styles.containerMain}`}
            onClick={() => nextPage()}
          >
            <div className={`${styles.containerControl}`}>
              <Image
                src={Next}
                alt="next"
                className={styles.imgControl}
              />
            </div>
          </div>
        </a>
      </div>
    </footer>
  );
};
