import styles from "../styles/Footer.module.css";

import { Kumar_One, Rubik } from "next/font/google";

const kumar_one = Kumar_One({
  weight: "400",
  subsets: ["latin"],
});

const rubik = Rubik({
  weight: "400",
  subsets: ["latin"],
});

export function Footer() {

  function anioActual() {
    return new Date().getFullYear();
  }

  return (
    <footer id={styles.footer}>
      <div id={styles.footerContainer} className="d-flex justify-content-center">
        <div className="row align-self-center text-center">
          <h6 id={styles.copyright}>
            <span id={styles.nameApp} className={kumar_one.className}>Bijouz</span>
            <span className={`${styles.simbolC} ${rubik.className}`}>©</span>
            <span className={rubik.className}>{anioActual()} All Rights Reserved</span>
          </h6>
        </div>
      </div>
    </footer>
  );
}