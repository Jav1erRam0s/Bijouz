export function darkMode() {
  /* App */
  const elemAppBody = document.getElementById("appBody");
  elemAppBody.style.backgroundColor = "#454545";

  /* Card Product */
  // [].slice.call(document.getElementsByClassName("cardProductBody")).forEach(cardProduct => {
  //   cardProduct.style.backgroundColor = "#642C6C";
  // });
  // [].slice.call(document.getElementsByClassName("cardTextBody")).forEach(cardText => {
  //   cardText.style.backgroundImage = "linear-gradient(180deg, rgba(50,17,54,0) 0%, rgba(50,17,54,1) 100%)";
  // });
}

export function lightMode() {
  /* App */
  const elemAppBody = document.getElementById("appBody");
  elemAppBody.style.backgroundColor = "whitesmoke";

  /* Card Product */
  // [].slice.call(document.getElementsByClassName("cardProductBody")).forEach(cardProduct => {
  //   cardProduct.style.backgroundColor = "#212529";
  // });
  // [].slice.call(document.getElementsByClassName("cardTextBody")).forEach(cardText => {
  //   cardText.style.backgroundImage = "linear-gradient(180deg, rgba(33,37,41,0) 0%, rgba(33,37,41,1) 100%)";
  // });
}
